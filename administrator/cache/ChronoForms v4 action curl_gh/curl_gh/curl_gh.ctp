<?php
/* ensure that this file is called by another file */
defined('_JEXEC') or die('Restricted access');

$action_name = 'curl_gh';
$base_dir = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_chronoforms'.DS.'form_actions'.DS.$action_name;
$lang =& JFactory::getLanguage();
$lang->load('com_chronoforms.'.$action_name, $base_dir);
?>
<div class="dragable" id="cfaction_curl_gh">cURL [GH]</div>
<!--start_element_code-->
<div
	class="element_code"
	id="cfaction_curl_gh_element"
>
	<label
		class="action_label"
		style="display: block; float:none!important;"
	>cURL [GH] : <?php echo $action_params['action_label']; ?></label>
	<input
		type="hidden"
		name="chronoaction[{n}][action_curl_gh_{n}_action_label]"
		id="action_curl_gh_{n}_action_label"
		value="<?php echo $action_params['action_label']; ?>"
	/>
	<input
		type="hidden"
		name="chronoaction[{n}][action_curl_gh_{n}_urlencode]"
		id="action_curl_gh_{n}_urlencode"
		value="<?php echo $action_params['urlencode']; ?>"
	/>
	<input
		type="hidden"
		name="chronoaction[{n}][action_curl_gh_{n}_target_url]"
		id="action_curl_gh_{n}_target_url"
		value="<?php echo htmlspecialchars($action_params['target_url']); ?>"
	/>
	<textarea
		name="chronoaction[{n}][action_curl_gh_{n}_content1]"
		id="action_curl_gh_{n}_content1"
		style="display:none"
	><?php echo $action_params['content1']; ?></textarea>
	<input
		type="hidden"
		name="chronoaction[{n}][action_curl_gh_{n}_header_in_response]"
		id="action_curl_gh_{n}_header_in_response"
		value="<?php echo $action_params['header_in_response']; ?>"
	/>
	<input
		type="hidden"
		id="chronoaction_id_{n}"
		name="chronoaction_id[{n}]"
		value="{n}"
	/>
	<input
		type="hidden"
		name="chronoaction[{n}][type]"
		value="curl_gh"
	/>
</div>
<!--end_element_code-->
<?php
$doc =& JFactory::getDocument();
$style = "
div#cfaction_{$action_name}_element_config {
	font-size: 100%;
}
div#cfaction_{$action_name}_element_config tt{
	font-size: 120%;
}
div#cfaction_{$action_name}_element_config span.gh_date {
	display: inline-block;
	width: 64px;
}
";
$doc->addStyleDeclaration($style);
echo "<div class='element_config' id='cfaction_{$action_name}_element_config' >";
echo $PluginTabsHelper->Header(
	array(
		'general' => JTEXT::_('CF_CU_GH_TAB_GENERAL'),
		'help' => JTEXT::_('CF_CU_GH_TAB_HELP'),
		'whats_new' => JTEXT::_('CF_CU_GH_TAB_WHATS_NEW')
	), $action_name.'_config_{n}'
);

echo $PluginTabsHelper->tabStart('general');
echo $HtmlHelper->input(
	'action_'.$action_name.'_{n}_action_label_config',
	array(
		'type' => 'text',
		'label' => JTEXT::_('CF_CU_GH_LABEL_ACTIONLABEL'),
		'class' => 'medium_input',
		'smalldesc' => JTEXT::_('CF_CU_GH_TIP_ACTIONLABEL')
	)
);
echo $HtmlHelper->input(
	'action_'.$action_name.'_{n}_target_url_config',
	array(
		'type' => 'text',
		'label' => JTEXT::_('CF_CU_GH_LABEL_TARGET_URL'),
		'class' => 'big_input',
		'smalldesc' => JTEXT::_('CF_CU_GH_TIP_TARGET_URL')
));
echo $HtmlHelper->input(
	'action_'.$action_name.'_{n}_content1_config',
	array(
		'type' => 'textarea',
		'label' => JTEXT::_('CF_CU_GH_LABEL_PFMAP'),
		'rows' => 15,
		'cols' => 50,
		'smalldesc' => JTEXT::_('CF_CU_GH_TIP_PFMAP')
));

echo $HtmlHelper->input(
	'action_'.$action_name.'_{n}_header_in_response_config',
	array(
		'type' => 'select',
		'label' => JTEXT::_('CF_CU_GH_LABEL_HEADRESP'),
		'options' => array(
			0 => JText::_('No'),
			1 => JText::_('Yes')
		),
		'smalldesc' => JTEXT::_('CF_CU_GH_TIP_HEADRESP')
));
echo $HtmlHelper->input('action_'.$action_name.'_{n}_urlencode_config',
	array(
		'type' => 'checkbox',
		'label' => JText::_('CF_CU_GH_LABEL_URLENCODE'),
		'class' => 'checkbox',
		'value' => '0',
		'rule' => 'bool',
		'smalldesc' => JText::_('CF_CU_GH_TIP_URLENCODE')
	)
);
echo $PluginTabsHelper->tabEnd();
echo $PluginTabsHelper->tabStart('help');
jimport('joomla.filesystem.file');
$tag =& $lang->getTag();
if ( !JFile::exists ( $base_dir.DS.'language'.DS.$tag.DS.$action_name.'_help.php' ) ) {
	$tag =& $lang->getDefault();
	if ( !JFile::exists ( $base_dir.DS.'language'.DS.$tag.DS.$action_name.'_help.php' ) ) {
		$tag = 'en-GB';
	}
}
include( $base_dir.DS.'language'.DS.$tag.DS.$action_name.'_help.php' );
echo $PluginTabsHelper->tabEnd();
echo $PluginTabsHelper->tabStart('whats_new');
$tag =& $lang->getTag();
if ( !JFile::exists ( $base_dir.DS.'language'.DS.$tag.DS.$action_name.'_whats_new.php' ) ) {
	$tag =& $lang->getDefault();
	if ( !JFile::exists ( $base_dir.DS.'language'.DS.$tag.DS.$action_name.'_whats_new.php' ) ) {
		$tag = 'en-GB';
	}
}
include( $base_dir.DS.'language'.DS.$tag.DS.$action_name.'_whats_new.php' );
echo $PluginTabsHelper->tabEnd();
?>
</div>