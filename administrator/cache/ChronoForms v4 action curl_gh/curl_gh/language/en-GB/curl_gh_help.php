<div>
    <div>This is an advanced cURL Action for ChronoForms v4 on Joomla! 1.5, 1.6, 1.7  &amp; 2.5. It has the same basic functions as the standard Email Action but adds more features and uses slightly different code in the configuration.</div>
    <h3>The General tab</h3>
    <h4>Action Label</h4>
    <ul>
        <li>You can enter a label for the cURL action in the Action Label box. This is optional but can be useful if you have a complex form with several cURL actions.</li>
    </ul>
    <h4>Target URL</h4>
    <ul>
        <li>Enter the URL that you want to send the data to in the Target URL box. This must either be a valid URL
        (complete with the http:// or https:// prefix); or a URL from the current site without the domain name; usually this will start with <tt>index.php</tt> and will be converted to a full URL before sending. If you leave the box empty it will default to the current site <?php echo JURI::base(); ?> If you include the 'query string' part of the the URL after a ? then the parameters and values there will be added to those in the Params/Fields map; if there are duplicated parameters the values from the Params/Fields map will be used.</li>
    </ul>
    <h4>Params/Fields map</h4>
    <ul>
        <li>The Params/Fields map is the main configuration box. Enter each data item that you want to send in this box using one line for each item. Each item takes the form of <tt>curl_parameter=some_value</tt>.
        <li>You can use values from the form results by using the 'curly bracket' syntax {input_name} e.g <tt>curl_parameter={input_name}</tt>.</li>
        <li>You can mix plain text and curly brackets in the same line e.g. <tt>curl_parameter=some text {input_name} some more text</tt></li>
        <li>Note: to set an empty parameter use the value NULL; lines with no value will be ignored.</li>
        <li>For advanced use only: You can also set custom cURL options in the Params/Fields map box. They must come after the cURL parameters and after a line with just ##CURLOPTS## on it. Entries take the form of CURLOPT=value e.g. CURLOPT_URL=http://example.com</li>
        <li>Here's an example of a set of entries:<br /><tt>banana=name<br />lettuce=email<br />apple={name}<br />beetroot={email}<br />##CURLOPTS##<br />CURLOPT_URL=http://example.com<br />CURLOPT_TIMEOUT=60</tt></li>
        <li>The Header in Response option will return the full header details on the cURL submission. Leave this set to 'No' unless you are de-bugging or know that you need the information as it may well show the whole of the responding web page!!</li>
    </ul>
</div>
