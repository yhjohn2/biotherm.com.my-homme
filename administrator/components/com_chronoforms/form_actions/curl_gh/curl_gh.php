<?php
/**
 * cURL [GH] action for ChronoForms v4
 * Copyright (c) 2011 Bob Janes aka Greyhead. All rights reserved.
 * Author: Greyhead (greyhead.net)
 *
 * @license GNU/GPL
 *
 * */

/* ensure that this file is called by another file */
defined( '_JEXEC' ) or die( 'Restricted access' );

if ( class_exists( 'CfactionCurlGH' ) ) {
    return;
}

class CfactionCurlGH
{
    var $action     = null;
    var $base_dir   = null;
    var $details    = null;
    var $events     = null;
    var $form       = null;
    var $formid     = null;
    var $formname   = null;
    var $group      = null;
    var $log        = null;
    var $logging    = null;
    var $params     = null;

    function CfactionCurlGH()
    {
        $this->action = 'curl_gh';
        $this->base_dir = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_chronoforms'.DS.'form_actions'.DS.$this->action;
        $lang =& JFactory::getLanguage();
        $lang->load( 'com_chronoforms.'.$this->action, $this->base_dir );
        $this->details  = array(
            'title' => JText::_( 'CF_CU_GH_TITLE' ),
            'tooltip' => JText::_( 'CF_CU_GH_TIP' )
        );
        $this->group = array(
            'id'    => 'gh',
            'title' => JText::_( 'CF_CU_GH_GROUP' )
        );
        $this->params = array(
            'header_in_response' => 0,
            'urlencode' => false,
            'action_label' => '',
            'target_url' => '',
            'content1' => '',
            'curlopts' => ''
        );
    }

    /**
     * [load description]
     *
     * @param [type]  $clear [description]
     * @return [type]
     */
    function load( $clear )
    {
        if ( $clear ) {
            $action_params = $this->params;
        }
        return array(
            'action_params' => $action_params
        );
    }
    /**
     * [run description]
     *
     * @param [type]  $form       [description]
     * @param [type]  $actiondata [description]
     * @return [type]
     */
    function run( $form, $actiondata )
    {
        $this->form =& $form;
        $params = new JParameter( $actiondata->params );
        if ( function_exists( 'curl_init' ) ) {
            $form->debug['cURL [GH]'][] = JText::_( 'CF_CU_GH_DEBUG_CURL_OK' );
        } else {
            $form->debug['cURL [GH]'][] = JText::_( 'CF_CU_GH_DEBUG_CURL_PROBLEM' );
            return;
        }
                // get the remaining parameters
        $skip_array = array(
            'content1'
        );
        foreach ( $this->params as $k => $v ) {
            if ( in_array( $k, $skip_array ) ) {
                continue;
            }
            $$k = trim( $params->get( $k, $v ) );
        }

        // get the name + value pairs and parse them
        $content     = explode( '##CURLOPTS##', $actiondata->content1 );
        $curl_values = $this->paramsToArray( $content[0], $urlencode);
        // get the Target URl
        if ( strpos( $target_url, '?' ) !== false ) {
            // the target URL has a query string
            $query      = explode( '?', $target_url );
            $target_url = $query[0];
            // parse the query string and merge with the curl values
            $query      = trim( $query[1] );
            if ( $query ) {
                $query       = str_replace( '&amp;', '&', $query );
                $query       = str_replace( '&', "\n", $query );
                $query       = $this->paramsToArray( $query , $urlencode);
                // values from the Params/Fields box over-ride values from URL
                $curl_values = array_merge( $query, $curl_values );
            }
        }
        // convert URL to  absolute form
        if ( strpos( $target_url, 'http' ) === false ) {
            $target_url = JURI::base() . $target_url;
        }
        $form->debug['cURL [GH]'][] = 'cURL Target URL: ' . $target_url;
        // build new query string from params
        foreach ( $curl_values as $k => $v ) {
            if ( is_array( $v ) ) {
                $v = print_r( $v, true );
            } else {
                $v = urldecode( $v );
            }
            $this->form->debug['cURL [GH]'][JText::_( 'CF_CU_GH_DEBUG_CURL_DATA' )][] = $k.' : '.$v;
        }
        $query                      = JURI::buildQuery( $curl_values );
        $form->debug['cURL [GH]'][] = 'cURL Values: ' . print_r( $query, true );
        // get CURLOPT settings
        $curlopts                   = array();
        if ( isset( $content[1] ) && $content[1] ) {
            $temp = explode( "\n", $content[1] );
            foreach ( $temp as $v ) {
                if ( !trim( $v ) ) {
                    continue;
                }
                $opt = explode( "=", $v, 2 );
                if ( trim( $opt[0] ) && trim( $opt[1] ) ) {
                    $curlopts[constant( $opt[0] )] = $opt[1];
                }
                unset( $opt );
            }
            unset( $temp );
            $form->debug['cURL [GH]'][] = 'cURL Options: ' . print_r( $curlopts, true );
        }
        $ch = curl_init( $target_url );
        curl_setopt( $ch, CURLOPT_HEADER, $params->get( 'header_in_response', 0 ) );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); // Returns response data instead of TRUE(1)
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $query ); // use HTTP POST to send form data
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        if ( count( $curlopts ) ) {
            curl_setopt_array( $ch, $curlopts );
        }
        $response                   = curl_exec( $ch ); //execute post and get results
        $form->debug['cURL [GH]'][] = 'cURL:' . print_r( $ch, true );
        $form->debug['cURL [GH]'][] = JText::_( 'CF_CU_GH_DEBUG_CURL_INFO' ).': ' . print_r( curl_getinfo( $ch ), true );
        curl_close( $ch );
        $form->debug['cURL [GH]'][] = 'Response: ' . print_r( $response, true );
        //add the response in the form data array
        $form->data['curl_gh']      = $response;
    }
    /**
     * Takes an ini string of name=value_name pairs
     * and returns an array of name & values
     *
     * @param string  $params    name=value_name string
     * @param string  $name      Name used for error messages
     * @param boolean $urlencode Urlencode the result
     * @return array/false
     */
    function paramsToArray( $params = '', $urlencode = false, $name = 'Parameter' )
    {
        if ( !$params ) {
            return false;
        }
        $list   = explode( "\n", trim( $params ) );
        $return = array();
        foreach ( $list as $item ) {
            $item = trim( $item );
            if ( !$item ) {
                $this->form->debug['cURL [GH]'][] = JText::sprintf( 'CF_CU_GH_DEBUG_CURL_EMPTY', $name );
                continue;
            }
            $fields_data = explode( "=", $item, 2 );
            if ( !isset( $fields_data[1] ) || !$fields_data[1] ) {
                $this->form->debug['cURL [GH]'][] = JText::sprintf( 'CF_CU_GH_DEBUG_CURL_NO_VALUE', $name, $fields_data[0] );
                continue;
            }
            $param = trim( $fields_data[0] );
            $value = trim( $fields_data[1] );
            if ( substr( $value, 0, 1 ) == '{' && substr( $value, -1, 1 ) == '}' && substr_count( $value, '{' ) == 1 ) {
                // this is a simple placeholder {input_name}
                $value = substr( $value, 1, strlen( $value ) - 2 );
                $value = trim( $value );
                // check for undefined value
                if ( !isset( $this->form->data[$value] ) ) {
                    $var = '';
                } else {
                    $var = $this->form->data[$value];
                }
                if ( is_array( $var ) ) {
                    $return[$param] = array();
                    foreach ( $var as $k => $v ) {
                        if ( $urlencode ) {
                            $v = urlencode( $v );
                        }
                        $return[$param][$k] = $v;
                    }
                } else {
                    $return[$param] = $var;
                }
            } elseif ( strpos( $value, '{' ) !== false ) {
                // this has mixed text and placeholders
                $found = preg_match_all( '#{(.*?)}#i', $value, $matches, PREG_SET_ORDER );
                if ( $found && $found > 0 ) {
                    foreach ( $matches as $match ) {
                        // Note: $match[0] = {text}, $match[1] = text
                        $v   = trim( $match[1] );
                        $val = $this->form->data[$v];
                        // explode array values into strings (one level only)
                        if ( is_array( $val ) ) {
                            $val = implode( ', ', $val );
                        }
                        $value = str_replace( $match[0], $val, $value );
                    }
                }
                $return[$param] = $value;
            } elseif ( $value == 'NULL' ) {
                // this is the special value NULL
                $return[$param] = '';
            } else {
                // this is a pain text string
                $return[$param] = $value;
            }
            if ( $urlencode && !is_array( $return[$param] ) ) {
                $return[$param] = urlencode( $return[$param] );
            }
        }
        return $return;
    }
}
?>
