<h4>Changelog</h4>
<ul>
	<li><span class='gh_date' >04 Mar 2012</span>: Added parsing of compound values like 'text {input_name} text'</li>
    <li><span class='gh_date' ></span>: Added debug output of name + value</li>
	<li><span class='gh_date' >05 Mar 2012</span>: Added parsing of the Target URL query string</li>
	<li><span class='gh_date' ></span>: Added debug output of name+value pairs</li>
	<li><span class='gh_date' >19 May 2012</span>: Added language support</li>
	<li><span class='gh_date' ></span>: Added URLencode option (recent versions set URLencoding on by default)</li>
</ul>
